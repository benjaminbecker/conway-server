# Conway's Game of Life
This is an implementation of Conway's game of life implemented in **React** (frontend) and **Python** (backend). The application is put together using **docker-compose**.

## Demo
A demo can be found here:

[http://gameoflife.benjaminbecker.net](http://gameoflife.benjaminbecker.net)

## Installation
Clone this repository to some folder on the target platform. CD into the directory containing `docker-compose.yml`. Then run
```shell
docker-compose build
docker-compose up &
```

## Modules
### Frontend
The source code for the **frontend** can be found here:

[https://gitlab.com/benjaminbecker/conway-frontend](https://gitlab.com/benjaminbecker/conway-frontend)

### Backend
The source code for the **backend** can be found here:

[https://gitlab.com/benjaminbecker/conway](https://gitlab.com/benjaminbecker/conway)

## Source of Data
The start configurations for Game of Life were downloaded from [LifeWiki](https://www.conwaylife.com/wiki/Main_Page).