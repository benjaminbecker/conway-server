from flask import Flask, send_file
from rle.parser import RLEParser
from flask_cors import CORS

app = Flask(__name__)
CORS(app)

import os
path = os.path.abspath(__file__)
dir_path = os.path.dirname(path)

@app.route('/list')
def list_all_files():
    return send_file(os.path.join(dir_path, 'rle_index.json'))

@app.route('/<filename>.rle')
def rle_as_json(filename):
    try:
        filepath = dir_path + '/rle/' + filename + '.rle'
        parser = RLEParser.from_file(filepath)
    except FileNotFoundError:
        return "File with name '{}' not found.".format(filename)
    rle_json = parser.json
    return rle_json


@app.route('/<filename>.svg')
def svg_image(filename):
    filepath = dir_path + '/svg/' + filename + '.svg'
    return send_file(filepath)


if __name__ == '__main__':
    app.run(host='0.0.0.0')