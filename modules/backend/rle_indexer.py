import os
import json
from rle.parser import RLEParser
import svgwrite

path = os.path.abspath(__file__)
dir_path = os.path.dirname(path)

def create_index_of_rle_files():
    index = dict()
    for filename in _rle_file_iterator():
        parser = RLEParser.from_file(filename)
        if parser.name and parser.width and parser.height:
            name = _prettify_name(parser.name)
            index.update({name: {'fn': os.path.basename(filename), 'w': parser.width, 'h': parser.height}})
    return index
        
def _rle_file_iterator():
    for root, dirs, files in os.walk(os.path.join(dir_path,'rle')):
        for filename in files:
            if filename.endswith('.rle'):
                yield(os.path.join(root, filename))

def _prettify_name(name):
    return name.replace('_', ' ').replace('.rle', '')

def _save_index_as_file(index):
    with open(os.path.join(dir_path, 'rle_index.json'), 'w') as json_file:
        json_file.write(json.dumps(index))

def create_svg_images():
    for filename in _rle_file_iterator():
        parser = RLEParser.from_file(filename)
        if parser.name and parser.width and parser.height and parser.width < 50 and parser.height < 50:
            _alive_list_to_svg(parser, filename.replace('rle', 'svg'))

def _alive_list_to_svg(parser, filename):
    max_of_width_height = max(parser.width, parser.height)
    view_box = f'0 0 {max_of_width_height} {max_of_width_height}'
    dwg = svgwrite.Drawing(filename, height=max_of_width_height, width=max_of_width_height, viewBox=(view_box))
    dwg.add(dwg.rect((0, 0), (max_of_width_height, max_of_width_height), fill='black'))
    for cell_alive in parser.alive_list:
        dwg.add(
            dwg.circle((cell_alive[0] + 0.5, cell_alive[1] + 0.5), 0.5, fill="#d6ce15")
        )
    dwg.save()
    


if __name__ == '__main__':
    print('Creating index ...')
    index = create_index_of_rle_files()
    _save_index_as_file(index)
    print('Creating svg images ...')
    create_svg_images()