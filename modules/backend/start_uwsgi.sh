#!/usr/bin/env bash
service nginx start
uwsgi --socket 127.0.0.1:3031 --wsgi-file rle_server.py --callable app --processes 4 --threads 2 --stats 127.0.0.1:9191